<?php
//include library
include('create_pdf/tcpdf.php');

//make TCPDF object
$pdf = new TCPDF('P','mm','A4');

//remove default header and footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

//add page
$pdf->AddPage();

//add content (student list)
//title
$pdf->SetFont('Helvetica','',14);
$pdf->Cell(190,10,"ECOLE SUPERIEURE AFRICAINE DES TICS",0,1,'C');

$pdf->SetFont('Helvetica','',8);
$pdf->Cell(190,5,"LISTE DES ETUDIANTS INSCRIT A LA BIBLIOTHEQUE",0,1,'C');

$pdf->SetFont('Helvetica','',10);
$pdf->Cell(35,5,"Bibliotheque ",0);
$pdf->Cell(160,5,": Ma biblio",0);
$pdf->Ln();
$pdf->Cell(30,5,"Responsable",0);
$pdf->Cell(160,5,": Prof. Jean Michelle",0);
$pdf->Ln();
$pdf->Ln(2);

//make the table
$html = "
	<table>
		<tr>
        <th >#</th>
        <th >code classe</th>
        <th >classe</th>
        <th >Nom</th>
        <th>Prenom</th>
        <th>sexe</th>
      
        <th >Matricule</th>
       
			
		</tr>
		";
//load the json data


require "db.php";
            $i=0;

            $etudiant=$pdo->prepare("SELECT * FROM ETUDIANT");
            $etudiant->execute();
            while($etu=$etudiant->fetch()){
                $i++;
                $n=$i;
                    //classe
                    $student=$pdo->prepare("SELECT Intitule FROM Class WHERE  Code_cl=?");
                    $student->execute([$etu['Code_cl']]);
                    $v_class=$student->fetch();
                                                                

//loop the data
                                    
                                        $html .= "
                                                <tr>
                                                    <td>".  $n ."</td>
                                                    <td>".  $etu['Code_cl'] ."</td> 
                                                  
                                                    <td>".  $v_class['Intitule']."</td>
                                                    <td>".  $etu["Nom"] ."</td>
                                                    <td>".  $etu['Prenom'] ."</td>
                                                    <td>".  $etu['Sexe'] ."</td>
                                                    <td>".  $etu['Matricule'] ."</td>
                                                   
                                                 
                                                   
                                                </tr>
                                                ";
                                    }		

$html .= "
	</table>
	<style>
	table {
		border-collapse:collapse;
	}
	th,td {
		border:1px solid #888;
	}
	table tr th {
		background-color:#888;
		color:#fff;
		font-weight:bold;
	}
	</style>
";
//WriteHTMLCell
$pdf->WriteHTMLCell(192,0,9,'',$html,0);	


//output
$pdf->Output();







