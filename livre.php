<?php 
if (session_status()==PHP_SESSION_NONE){
  session_start();
}

if (!empty($_POST['submit'])){
  $code_livre=htmlspecialchars("QWE123");
  $date_s= date('y-m-d ');
  $date_r= date('y-m-d ');
  require "db.php";

      //VERIFIER LE NOMBRE DE LIVRE EN SOCKE

      $v_quantite=$pdo->prepare("SELECT quantite  FROM Livre WHERE Code_livre=?");
      $v_quantite->execute([$code_livre]);
      $v=$v_quantite->fetch();
      print_r( $v['quantite']);
      $new_quatite=$v['quantite']-1;

        if($new_quatite>-1){

          //inserer la nouvelle quantie

          $quantie=$pdo->prepare("UPDATE Livre SET quantite=? WHERE Code_livre=?");
          $quantie->execute([$new_quatite,$code_livre]);

          //emprunter le livre
          $req= $pdo->prepare("INSERT INTO emprunt SET Code_livre=?, d_retour=?,d_sortie=?,Matricule=?" );
          $req->execute([$code_livre,$date_r,$date_s,$_SESSION['matricule']]);
          $successtock="livre emprunte avec succes  ";
        


        }else{
          $errorstock="stock epuise";
        }

          
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Moi je lis</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Mamba - v4.0.2
  * Template URL: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar =======
  <section id="topbar" class="d-flex align-items-center">
    <div class="container d-flex justify-content-center justify-content-md-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope-fill"></i><a href="mailto:contact@example.com">info@example.com</a>
        <i class="bi bi-phone-fill phone-icon"></i> +1 5589 55488 55
      </div>
      <div class="social-links d-none d-md-block">
        <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
        <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
        <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
        <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
      </div>
    </div>
  </section> -->

  <!-- ======= Header ======= -->
  <?php require "navbar.php" ?>
  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <!-- Slide 1 -->
          <div class="carousel-item active" style="background-image: url('assets/6607.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Bienvenue <span>les amoureux du livre</span></h2>
                <p class="animate__animated animate__fadeInUp"><h3>une heure de lecture est le souverain remede contre les degouts de la vie..</h3></p>
                <p>Montesquieux</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Allez plus loin </a>
              </div>
            </div>
          </div>

          <!-- Slide 2 -->
          <div class="carousel-item" style="background-image: url('assets/6263.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Lire n'a jamais ete aussi passionant</h2>
                <p class="animate__animated animate__fadeInUp">Vous pouvez Emprunter,acheter </p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Allez plus loin</a>
              </div>
            </div>
          </div>

          <!-- Slide 3 -->
          <div class="carousel-item" style="background-image: url('assets/img/slide/slide-3.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Asseyez vous confortablement !!Top,c'est parti!</h2>
                <p class="animate__animated animate__fadeInUp">Fiction,policier,romance,science...Vous y trouverez forcement votre place</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Allez plus loin</a>
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
          <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
          <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->

 
    <section id="team" class="team">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Lecteur du mois</h2>
          <p>Presentation des meilleurs lecteur de moi je lis du mois. Veux tu avoir egalement ton image ici.Alors quesct ce tu attends pour emprunter un livre</p>
        </div>

        <div class="row">

          <div class="col-xl-3 col-lg-4 col-md-6" data-aos="fade-up">
            <div class="member">
              <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>BAMBA CHARLES</h4>
                <span>ETUDIANT EN SITW</span>
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
            <div class="member">
              <div class="pic"><img src="assets/img/team/team-2.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Suzanne Didia</h4>
                <span>delegue en master securite</span>
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
            <div class="member">
              <div class="pic"><img src="assets/img/team/team-3.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Irie Doubi</h4>
                <span>etudiant en big data</span>
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
            <div class="member">
              <div class="pic"><img src="assets/img/team/team-4.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Fanta Kamate</h4>
                <span>etudiante en info</span>
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
 <!-- ======= Our Portfolio Section ======= -->
 <section id="portfolio" class="portfolio section-bg">
  <div class="container" data-aos="fade-up" data-aos-delay="100">

    <div class="section-title">
      <h2>Ces oeuvres pourront vous interresser</h2>
      <div>   <?php if (isset($errorstock))  {?><h6 class="alert alert-danger" role="alert"><?= $errorstock ?>  </h6> <?}?>
      <div>   <?php if (isset($successtock))  {?><h6 class="alert alert-success" role="alert"><?= $successtock ?>  </h6> <?}?>
  
<!-- A Search form is another common non-linear way to navigate through a website. -->

<form role="search">
          <input type="search" name="q" placeholder="chercher,titre ,auteur ,genre" aria-label="Search through site content">
          <input type="submit" class="filter-active" value="Go!">
        </form>
    <div class="row">
      <div class="col-lg-12">
        <ul id="portfolio-flters">
          <li data-filter="*" class="filter-active">Tout</li>
          <li data-filter=".filter-science">science</li>
          <li data-filter=".filter-negritude">negritude</li>
          <li data-filter=".filter-informatique">informatique</li>
        </ul>
      </div>
    </div>

    <div class="row portfolio-container">

      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <div class="portfolio-wrap">
        <form method="POST" >
          <img src="assets/livre/41y74DnCvxL._AC_UL320_.jpg" class="img-fluid" alt="">
          <div class="portfolio-info">
          
              <p name="Code_livre">QWE123</input>
            <p name='Prix'>3000 fcfa</p>
            <div class="portfolio-links">
              <a href="assets/livre/41y74DnCvxL._AC_UL320_.jpg" data-galleryery="portfolioGallery" class="portfolio-lightbox" title="App 1"><i class="bi bi-plus"></i></a>
              <?php if (isset($_SESSION['nomU'])):?>
          
              <input class="btn btn-info"  type="submit" name="submit" value="emprunter">
             <?php endif ?>
            </div>
          </div>
          </form>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-web">
        <div class="portfolio-wrap">
          <img src="assets/livre/716f1Rtx1CL._AC_UL320_.jpg" class="img-fluid" alt="">
          <div class="portfolio-info">
          <p name="Code_livre">QWE004</input>
            <p name='Prix'>8000 fcfa</p>
            <div class="portfolio-links">
              <a href="assets/livre/716f1Rtx1CL._AC_UL320_.jpg" data-galleryery="portfolioGallery" class="portfolio-lightbox" title="Web 3"><i class="bi bi-plus"></i></a>
              <?php if (isset($_SESSION['nomU'])):?>
          
          <input class="btn btn-info"  type="submit" name="submit" value="emprunter">
         <?php endif ?>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <div class="portfolio-wrap">
          <img src="assets/livre/71HKqd-MwaL._AC_UL320_.jpg" class="img-fluid" alt="">
          <div class="portfolio-info">
          <p name="Code_livre">QWE000</input>
            <p name='Prix'>7500 fcfa</p>
            <div class="portfolio-links">
              <a href="assets/livre/71HKqd-MwaL._AC_UL320_.jpg" data-galleryery="portfolioGallery" class="portfolio-lightbox" title="App 2"><i class="bi bi-plus"></i></a>
              <?php if (isset($_SESSION['nomU'])):?>
          
          <input class="btn btn-info"  type="submit" name="submit" value="emprunter">
         <?php endif ?>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
        <div class="portfolio-wrap">
          <img src="assets/livre/71jocup+ekL._AC_UL320_.jpg" class="img-fluid" alt="">
          <div class="portfolio-info">
            <h4>Card 2</h4>
            <p>Card</p>
            <div class="portfolio-links">
              <a href="assets/livre/71jocup+ekL._AC_UL320_." data-galleryery="portfolioGallery" class="portfolio-lightbox" title="Card 2"><i class="bi bi-plus"></i></a>
              <?php if (isset($_SESSION['nomU'])):?>
          
          <input class="btn btn-info"  type="submit" name="submit" value="emprunter">
         <?php endif ?>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-web">
        <div class="portfolio-wrap">
          <img src="assets/livre/moi_momo_14_ans_ivoirien_et_plus_jeune_bachelier_de_france-12406-264-432.jpg" class="img-fluid" alt="">
          <div class="portfolio-info">
            <h4>Web 2</h4>
            <p>Web</p>
            <div class="portfolio-links">
              <a href="assets/livre/moi_momo_14_ans_ivoirien_et_plus_jeune_bachelier_de_france-12406-264-432.jpg" data-galleryery="portfolioGallery" class="portfolio-lightbox" title="Web 2"><i class="bi bi-plus"></i></a>
              <?php if (isset($_SESSION['nomU'])):?>
          
          <input class="btn btn-info"  type="submit" name="submit" value="emprunter">
         <?php endif ?>  </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <div class="portfolio-wrap">
          <img src="assets/livre/81+5tjtuBBL._AC_UL320_.jpg" class="img-fluid" alt="">
          <div class="portfolio-info">
            <h4>App 3</h4>
            <p>App</p>
            <div class="portfolio-links">
              <a href="assets/livre/81+5tjtuBBL._AC_UL320_.jpg" data-galleryery="portfolioGallery" class="portfolio-lightbox" title="App 3"><i class="bi bi-plus"></i></a>
              <?php if (isset($_SESSION['nomU'])):?>
          
          <input class="btn btn-info"  type="submit" name="submit" value="emprunter">
         <?php endif ?>  </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
        <div class="portfolio-wrap">
          <img src="assets/livre/8139FJxyvjL._AC_UL320_.jpg" class="img-fluid" alt="">
          <div class="portfolio-info">
            <h4>Card 1</h4>
            <p>Card</p>
            <div class="portfolio-links">
              <a href="assets/livre/8139FJxyvjL._AC_UL320_.jpg" data-galleryery="portfolioGallery" class="portfolio-lightbox" title="Card 1"><i class="bi bi-plus"></i></a>
              <?php if (isset($_SESSION['nomU'])):?>
          
          <input class="btn btn-info"  type="submit" name="submit" value="emprunter">
         <?php endif ?>    </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
        <div class="portfolio-wrap">
          <img src="assets/livre/negre.jpg" class="img-fluid" alt="">
          <div class="portfolio-info">
            <h4>Card 3</h4>
            <p>Card</p>
            <div class="portfolio-links">
              <a href="assets/livre/negre.jpg" data-galleryery="portfolioGallery" class="portfolio-lightbox" title="Card 3"><i class="bi bi-plus"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bi bi-link"></i></a>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-web">
        <div class="portfolio-wrap">
          <img src="assets/livre/l.jpg" class="img-fluid" alt="">
          <div class="portfolio-info">
            <h4>Web 3</h4>
            <p>Web</p>
            <div class="portfolio-links">
              <a href="assets/livre/l.jpg" data-galleryery="portfolioGallery" class="portfolio-lightbox" title="Web 3"><i class="bi bi-plus"></i></a>
              <a href="portfolio-details.html" title="More Details"><i class="bi bi-link"></i></a>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
</section><!-- End Our Portfolio Section -->



  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Jean Deya</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/ -->
        Designed by <a href="/">Delor</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/purecounter/purecounter.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>