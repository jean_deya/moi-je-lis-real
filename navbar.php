  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center">

      <div class="logo me-auto">
        <h1><a href="index.html">Moi je lis</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Acceuil</a></li>
          <li><a class="nav-link scrollto" href="#about">A propos</a></li>
          <li><a class="nav-link scrollto" href="#services">Services</a></li>
        
         
          <li class="dropdown"><a href="#"><span>Connexion</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="login.php">Espace Etudiant</a></li>
              <li><a href="loginadmin.php">Espace gestionnaire</a></li>>
            </ul>
          </li>
          <li class="dropdown"><a href="livre.php"><span>Literrature</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Auteurs connus</a></li>
              <li><a href="#">Dernieres sorties</a></li>
              <li><a href="#">Auteurs Celebres</li>
            </ul>
          </li>

          <?php if (isset($_SESSION['nomU'])):?>
          <li class="dropdown"><a href="#"><h6> <?= $_SESSION['nomU']; ?></h6></a>
          <ul>
              <li><a href=profil.php>Profil</a></li>
              <li><a href="#">Editer profil</a></li>
              <li><a href="profil.php">Mes livres</a></li>
              <li><a href="deconnexion.php">Se deconnecter</a></li>
            </ul>
          </li>
          <?php endif?>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->