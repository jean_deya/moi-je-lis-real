-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 19, 2021 at 01:11 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `BIBLIOTHEQUE`
--

-- --------------------------------------------------------

--
-- Table structure for table `ADMINISTRATEUR`
--

CREATE TABLE `ADMINISTRATEUR` (
  `Matricule` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Nom` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ADMINISTRATEUR`
--

INSERT INTO `ADMINISTRATEUR` (`Matricule`, `Nom`) VALUES
('777-DELOR', 'Deya');

-- --------------------------------------------------------

--
-- Table structure for table `Class`
--

CREATE TABLE `Class` (
  `Code_cl` int(11) NOT NULL,
  `Intitule` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Effectif` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Class`
--

INSERT INTO `Class` (`Code_cl`, `Intitule`, `Effectif`) VALUES
(7, 'SRIT 2A', 1),
(10, 'Master sigl 2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `emprunt`
--

CREATE TABLE `emprunt` (
  `d_sortie` date NOT NULL,
  `d_retour` date NOT NULL,
  `Matricule` varchar(50) NOT NULL,
  `Code_livre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `emprunt`
--

INSERT INTO `emprunt` (`d_sortie`, `d_retour`, `Matricule`, `Code_livre`) VALUES
('2021-03-06', '2021-03-03', '21-edu01', 'QWE123'),
('2021-03-10', '2021-03-30', '21-edu01', 'QWE123'),
('2021-03-07', '2021-03-07', '21-edu01', 'QWE123'),
('2021-03-07', '2021-03-07', '21-edu01', 'QWE123'),
('2021-03-07', '2021-03-07', '21-edu01', 'QWE123'),
('2021-03-07', '2021-03-07', '21-edu01', 'QWE123'),
('2021-03-07', '2021-03-07', '21-edu01', 'QWE123'),
('2021-03-07', '2021-03-07', '21-edu01', 'QWE123'),
('2021-03-07', '2021-03-07', '21-edu01', 'QWE123'),
('2021-03-08', '2021-03-08', '21-edu002', 'QWE123'),
('2021-03-08', '2021-03-08', '21-edu002', 'QWE123'),
('2021-03-08', '2021-03-08', '21-edu002', 'QWE123'),
('2021-03-08', '2021-03-08', '21-edu002', 'QWE123'),
('2021-03-08', '2021-03-08', '21-edu002', 'QWE123'),
('2021-03-08', '2021-03-08', '21-edu002', 'QWE123'),
('2021-03-08', '2021-03-08', '21-edu002', 'QWE123'),
('2021-03-10', '2021-03-10', '21-edu0010', 'QWE123'),
('2021-03-10', '2021-03-10', '21-edu0010', 'QWE123'),
('2021-03-10', '2021-03-10', '21-edu0010', 'QWE123'),
('2021-04-14', '2021-04-14', '21-edu0010', 'QWE123'),
('2021-04-14', '2021-04-14', '21-edu0010', 'QWE123'),
('2021-04-14', '2021-04-14', '21-edu0010', 'QWE123'),
('2021-04-14', '2021-04-14', '21-edu0010', 'QWE123');

-- --------------------------------------------------------

--
-- Table structure for table `ETUDIANT`
--

CREATE TABLE `ETUDIANT` (
  `Matricule` varchar(50) NOT NULL,
  `Nom` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Sexe` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Prenom` varchar(20) NOT NULL,
  `Code_cl` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ETUDIANT`
--

INSERT INTO `ETUDIANT` (`Matricule`, `Nom`, `Sexe`, `Prenom`, `Code_cl`) VALUES
('21-edu0010', 'jean', 'feminin', 'delor', 7),
('21-edu002', 'Didia', 'feminin', 'Suzanne', 10),
('21-edu008', 'ouattara', 'feminin', 'ablo', 7),
('21-edu01', 'Lebouath', 'masculin', 'jean', 10),
('21-edu11', 'yao', 'm', 'jean', 7),
('21-edu12', 'DIARRASO', 'F', 'SARA', 7),
('21-edu13', 'KONE', 'M', 'DIARRA', 10),
('21-edu14', 'KOUAKOU', 'F', 'JEANNE', 10),
('21-edu15', 'COYA', 'F', 'VIRGINE', 7),
('21-edu16', 'BAMBA', 'M', 'CHARLES', 10),
('3434r', 'g', 'masculin', 'd', 7),
('bonaergea', 'reet', 'feminin', 'trg', 7),
('dbhfxhn556', 'delor', 'm', 'deya', 7),
('df', 'fd', 'masculin', 'fd', 10),
('f', 'f', 'feminin', 'f', 7),
('ff', 'fff', 'masculin', 'ff', 7),
('ffd', 'fgggghh', 'masculin', 'fd', 7),
('fhgfr', 'tgthrb', 'feminin', 'rtfgh', 7),
('gtb', 'fg', 'masculin', 'tbh', 7),
('hhtrjh', 'rh', 'masculin', 'rtrrrfdgr', 7),
('MjEtZWR1MDAyMg==', 'bmlub24=', 'bWFzY3VsaW4=', 'ZnJlZA==', 10),
('ODDOOOOO', 'OOOO', 'masculin', 'OOO', 7),
('thrhe', 'hgnr', 'feminin', 'tyhst', 7);

-- --------------------------------------------------------

--
-- Table structure for table `Livre`
--

CREATE TABLE `Livre` (
  `Genre` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Prix` int(100) NOT NULL,
  `Titre` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Auteur` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Code_livre` varchar(30) NOT NULL,
  `quantite` int(11) NOT NULL,
  `couverture` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Livre`
--

INSERT INTO `Livre` (`Genre`, `Prix`, `Titre`, `Auteur`, `Code_livre`, `quantite`, `couverture`) VALUES
('G', 5, 'G', 'G', 'biblio3', 5, 'upload/couverture/albert.jpeg'),
('Informatique', 12000, 'Apprendre php', 'Valhein roger', 'QWE000', 5, 'upload/couverture/sec.jpeg'),
('Informatique', 174500, 'Big Data and Security', 'Steve McDi', 'QWE004', 5, 'upload/couverture/sec.jpeg'),
('ROMAN', 15000, 'L\'ETRANGER', 'ALBERT CAMUS', 'QWE123', 0, 'upload/couverture/albert.jpeg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ADMINISTRATEUR`
--
ALTER TABLE `ADMINISTRATEUR`
  ADD PRIMARY KEY (`Matricule`);

--
-- Indexes for table `Class`
--
ALTER TABLE `Class`
  ADD PRIMARY KEY (`Code_cl`);

--
-- Indexes for table `emprunt`
--
ALTER TABLE `emprunt`
  ADD KEY `Code_livre` (`Code_livre`),
  ADD KEY `Matricule` (`Matricule`);

--
-- Indexes for table `ETUDIANT`
--
ALTER TABLE `ETUDIANT`
  ADD PRIMARY KEY (`Matricule`),
  ADD KEY `Code_cl` (`Code_cl`);

--
-- Indexes for table `Livre`
--
ALTER TABLE `Livre`
  ADD PRIMARY KEY (`Code_livre`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Class`
--
ALTER TABLE `Class`
  MODIFY `Code_cl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `emprunt`
--
ALTER TABLE `emprunt`
  ADD CONSTRAINT `emprunt_ibfk_1` FOREIGN KEY (`Code_livre`) REFERENCES `Livre` (`Code_livre`),
  ADD CONSTRAINT `emprunt_ibfk_2` FOREIGN KEY (`Matricule`) REFERENCES `ETUDIANT` (`Matricule`);

--
-- Constraints for table `ETUDIANT`
--
ALTER TABLE `ETUDIANT`
  ADD CONSTRAINT `ETUDIANT_ibfk_1` FOREIGN KEY (`Code_cl`) REFERENCES `Class` (`Code_cl`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
