<?php 
session_start();
   require 'requete.php';
   if(isset($_POST['submit'])){    
        require 'db.php';
        $nom=htmlspecialchars( $_POST['nomU']);
        $prenom=htmlspecialchars($_POST['PrenomU']);
        $matricule=htmlspecialchars($_POST['MatriculeU']);
        $gender=htmlspecialchars($_POST["gender"]);
        $Code_cl=htmlspecialchars($_POST['i']);

            if((!empty($nom)) && (!empty($prenom)) && (!empty($matricule)) && (!empty($gender))&& (!empty($Code_cl)) ){

              
                //verifier matricule

                $v_matricule=$pdo->prepare("SELECT Matricule FROM ETUDIANT WHERE  Matricule=? ") ;
                $v_matricule->execute([$matricule]) ;
                $v=$v_matricule->fetch();
                if ($v){
                  $errormatricule="ce matricule existe deja";
    
                }else { 
                   //prendre l'id de la classe concernee
                   $id =$pdo->prepare("SELECT Code_cl FROM Class WHERE  Intitule=? ") ;
                   $id->execute( [ $Code_cl]);
                   //enregister l etudiant

                       while($d=$id->fetch()){  $id1=$d['Code_cl'];
                           $req= $pdo->prepare("INSERT INTO ETUDIANT SET Nom=?, Prenom=?,Matricule=?,Sexe=? ,Code_cl=?");
                           $req->execute([$nom,$prenom,$matricule,$gender,$id1]);
                           $succesmessage='INSCRIPTION EFFECTUE AVEC SUCCES';
                           }
           
                     // header('Location:index.php');
             
                }
            
                
            }else{
             
                $errormessage="Veuilez remplir tous les champs...";
            }
    }
    ?>

<?php
if (session_status()==PHP_SESSION_NONE){
    session_start();
  }
 
require "db.php";

if (isset($_POST['submit2'])){
    var_dump($_POST['MatriculeA']);
        $nomA=htmlspecialchars( $_POST['nomA']);
        $matriculeA=htmlspecialchars($_POST['MatriculeA']);
   // echo $nom;
            if((!empty($nomA)) && (!empty($matriculeA))  ){

                $v_nom=$pdo->prepare("SELECT Nom  FROM ADMINISTRATEUR WHERE  Nom=?");
                $v_nom->execute([$nomA]);
                $n=$v_nom->fetch();
                    if($n){
                       // print_r($n['Nom']);
                        $N=$n['Nom'];
                        $v_matricule=$pdo->prepare("SELECT Matricule  FROM ADMINISTRATEUR WHERE  Matricule=? and Nom=?");
                        $v_matricule->execute([$matriculeA,$N]);
                        $m=$v_matricule->fetch();
                        if($m){
                            session_start();
                            $connexion="vous etes connecte !";
                            echo "vous etes connecte";
                            $_SESSION['nomA']=$nomA;
                            $_SESSION['matriculeA']=$matriculeA;
                           header('Location:dashboardGest.php');

                        }else{
                            $errormatricule="matricule incorrecte";
                        //    echo "erreur matricule";
                        }
                    }else{
                        $errornom="nom incorrecte";
                       // echo "erreur nom";
                    }
            }


}else{
    $errormessage="Veuilez remplir tous les champs...";
}


?>

<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Login & Signup Form | CodingNepal</title> -->
    <link rel="stylesheet" href="login.css">
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
    .slide {
        background-image: radial-gradient(circle at 13.66% 56.41%, #586e2b 0, #486824 16.67%, #325e1b 33.33%, #11520f 50%, #004505 66.67%, #003c00 83.33%, #003500 100%);
      background-color: blue;
        padding:4px;
        color:white;
        margin:10px 60px 0px 68px;
    }
  </style>
  </head>
  <body>
    <div class="wrapper">
      <div class="title-text">
      
        <div class="title login">Formulaire<div>  
          <?php if (isset($errormessage)) 
            {?><h6 class="alert alert-danger" role="alert"><?= $errormessage ?> 
              </h6> <?}?>
      
          <div class="form-container">
            <label for="login" class="slide ">se connecter</label>
          </div>
       </div>
       

    <div class="form-inner">
          <form method="POST" class="login">
              <div class="field">
                <input class="input--style-1" type="text" placeholder="Nom" name="nomA">
          <?php if (isset( $errornom))  {?><div style="color:red;size:120px"><?=  $errornom ?>  </div> <?}?><p>
              </div>

              <div class="field">
                <input type="text" placeholder="MatriculeA" name="MatriculeA" required>
                <p></p>
                <?php if (isset($errormatricule))  {?><div style="color:red;size:120px"><?= $errormatricule ?>  </div> <?}?><p>
                
              </div>

              <div class="pass-link"><a href="#">matricule oublie?</a></div>
              <div class="field btn">
              <div class="btn-layer">
          </div>

            <input type="submit" name="submit2" value="Login">
                </div>
          <div class="signup-link"> Espace administrateur </div>
          </form>

         
</div>
</div>
</div>
<script>
      const loginText = document.querySelector(".title-text .login");
      const loginForm = document.querySelector("form.login");
      const loginBtn = document.querySelector("label.login");
      const signupBtn = document.querySelector("label.signup");
      const signupLink = document.querySelector("form .signup-link a");
      signupBtn.onclick = (()=>{
        loginForm.style.marginLeft = "-50%";
        loginText.style.marginLeft = "-50%";
      });
      loginBtn.onclick = (()=>{
        loginForm.style.marginLeft = "0%";
        loginText.style.marginLeft = "0%";
      });
      signupLink.onclick = (()=>{
        signupBtn.click();
        return false;
      });
    </script>

     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  </body>
</html>
